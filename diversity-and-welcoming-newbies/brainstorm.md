# Ideias

## Sugestões de melhorias:

* palestras antes do sprint para dizer a novatos o que é o Debian.
* Ter um workshop para newcomers, esquema de mentores.
* Ter algum booth (mesa) com algum banner dizendo "Não conhece o Debian ou é novato? Fale conosco".
* No credenciamento ter pessoas que perguntam se a pessoa é novata ou não e direcionar/guiar a pessoa.
* Distribuir panfletos para quem está na universidade sobre o evento (durante o evento).
* Colocar alguma fala na abertura sobre novatos na comunidade.
* Divulgar as bolsas para participação da DebConf 2019 em projetos de mulheres no Brasil.
* Tentar parcerias para tradução de inglês para português e Libras durante a DebConf 2019.
* Trazer a Sheila do wordpress para fazer fala sobre como promover novas entradas.
* Espaço para receber feedbacks (talvez anônimo).
* Para DebConf 2019, fazer uma aproximação com as mulheres da Universidade local.
   
## Mensagem do Lamb propondo uma ideia para ajudar novatos na DebConf

https://lists.debian.org/debconf-discuss/2017/12/msg00000.html

## Textos do  Eric Holscher sobre esse assunto:

* http://ericholscher.com/blog/2017/dec/2/breaking-cliques-at-events/
* http://ericholscher.com/blog/2017/aug/2/pacman-rule-conferences/
* http://www.writethedocs.org/organizer-guide/confs/welcome-wagon/
* http://www.writethedocs.org/conf/na/2017/welcome-wagon/
    
Nota: Lembrando que as pessoas que estão organizando os eventos do Debian são voluntários e não são profissionais em produção de eventos. E todos são co responsáveis e estamos juntos nesse processo de aprendizagem coletivo em tornar os eventos do Debian cada vez melhor. 

* Planejar alguma forma de publicitar mais que o evento é colaborativo, que está sempre aberto e necessitando de pessoas para fazer acontecer, deixar claro que qualquer um pode ajudar e que qualquer ajuda produtiva é bem-vinda. 
* Trazer mais pessoas que estejam dispostas a colaborar de forma produtiva.
* Explicitar mais a política anti-assédio.